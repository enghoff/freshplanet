﻿using UnityEngine.Events;

namespace FreshPlanet
{
    public class Observable<T0> : UnityEvent<T0>
    {
        public UnityAction Refresh;

        private class Singles<T1> : UnityEvent<T1> { }
        private Singles<T0> _singles = new Singles<T0>();
        private bool _initialised;

        private T0 _value;
        public T0 value
        {
            get { return _value; }
            set
            {
                _initialised = true;
                Invoke(_value = value);
            }
        }

        public Observable()
        {
            Refresh = new UnityAction(() => Invoke(value));

            base.AddListener((value) =>
            {
                _singles.Invoke(value);
                _singles.RemoveAllListeners();
            });
        }

        public void AddListener(UnityAction<T0> call, bool single = false, bool deferred = false)
        {
            if (single) _singles.AddListener(call);
            else base.AddListener(call);

            if (_initialised && !deferred) call.Invoke(value);
        }
    }
}