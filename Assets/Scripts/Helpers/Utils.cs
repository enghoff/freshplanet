﻿using UnityEngine.Events;

using System.Collections.Generic;

namespace FreshPlanet
{
	public static class Utils
	{
		public static void ForEach<T>(this IEnumerable<T> enumerable, UnityAction<T> handler)
		{
			foreach (T item in enumerable)
				handler(item);
		}
	}
}