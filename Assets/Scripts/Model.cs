﻿using System.Collections.Generic;

namespace FreshPlanet.Model
{
    public class Config : List<Quiz> { };

    public class Quiz
    {
        public string id;
        public Question[] questions;
        public string playlist;
    }

    public class Question
    {
        public string id;
        public Choice[] choices;
        public Song song;
        public int answerIndex;
    }

    public class Choice
    {
        public string artist;
        public string title;

        public override string ToString() => $"{artist} / {title}";
    }

    public class Song
    {
        public string id;
        public string title;
        public string artist;
        public string picture;
        public string sample;
    }

    public class Results : List<Result> { }

    public class Result
    {
        public Choice Correct;
        public Choice Player;

        public bool isCorrect => Correct == Player;
    }
}