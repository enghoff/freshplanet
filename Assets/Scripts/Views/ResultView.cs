﻿using UnityEngine;

using System.Linq;

using TMPro;

using FreshPlanet.Controllers;
using FreshPlanet.Model;

namespace FreshPlanet.Views
{
    public class ResultView : View
    {
        [SerializeField] private TMP_Text _footer;
        [SerializeField] private RectTransform _content;
        [SerializeField] private ResultEntry _prefab;

        private ResultView Initialize(Results results)
        {
            _footer.text = $"You got {results.Count(result => result.isCorrect)} songs right";
            _content.GetComponentsInChildren<ResultEntry>().ForEach(result => Destroy(result.gameObject));

            results.ForEach(result => Instantiate(_prefab, _content).Initialize(result));

            return this;
        }

        public static void Show(Results results) => ViewController.Get<ResultView>().Initialize(results).Show();
    }
}