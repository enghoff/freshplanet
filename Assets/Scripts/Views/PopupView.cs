﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;

using FreshPlanet.Controllers;

namespace FreshPlanet.Views
{
    public class PopupView : View
    {
        [SerializeField] private TMP_Text _success;
        [SerializeField] private Graphic _background;
        [SerializeField] private Color _correctAnswer;
        [SerializeField] private Color _incorrectAnswer;

        private PopupView Initialize(bool isCorrect)
        {
            _success.text = isCorrect ? "You got it!" : "Not Right :(";
            _background.color = isCorrect ? _correctAnswer : _incorrectAnswer;

            return this;
        }

        public static void Show(bool success) => ViewController.Get<PopupView>().Initialize(success).Show();

        public void OnClick() => GameController.NextQuestion();
    }
}