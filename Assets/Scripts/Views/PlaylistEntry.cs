﻿using UnityEngine;

using TMPro;

using FreshPlanet.Controllers;

namespace FreshPlanet.Views
{
    public class PlaylistEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text _title;

        private Model.Quiz _quiz;

        public void Initialize(Model.Quiz quiz)
        {
            _quiz = quiz;
            _title.text = quiz.playlist;
        }

        public void OnClick() => GameController.Play(_quiz);
    }
}