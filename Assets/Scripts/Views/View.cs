﻿using UnityEngine;
using FreshPlanet.Controllers;

namespace FreshPlanet.Views
{
    public class View : MonoBehaviour
    {
        public void Show() => ViewController.Show(this);
    }
}