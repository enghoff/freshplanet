﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;

using FreshPlanet.Model;

namespace FreshPlanet.Views
{
    public class ResultEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text _correct;
        [SerializeField] private TMP_Text _player;
        [SerializeField] private Graphic _background;
        [SerializeField] private Color _correctAnswer;
        [SerializeField] private Color _incorrectAnswer;

        public void Initialize(Result result)
        {
            _correct.text = $"Correct: {result.Correct}";
            _player.text = $"You said: {result.Player}";
            _background.color = result.isCorrect ? _correctAnswer : _incorrectAnswer;
        }
    }
}