﻿using UnityEngine;

using TMPro;

using FreshPlanet.Controllers;
using FreshPlanet.Model;

namespace FreshPlanet.Views
{
    public class ChoiceEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text _artist;
        [SerializeField] private TMP_Text _title;

        private Choice _choice;

        public void Initialize(Choice choice)
        {
            _choice = choice;
            _artist.text = choice.artist;
            _title.text = choice.title;
        }

        public void OnClick() => GameController.Answer(_choice);
    }
}