﻿using UnityEngine;

using FreshPlanet.Controllers;

namespace FreshPlanet.Views
{
    public class PlaylistView : View
    {
        [SerializeField] private RectTransform _content;
        [SerializeField] private PlaylistEntry _prefab;

        private void Start() => DataController.Config.ForEach(quiz => Instantiate(_prefab, _content).Initialize(quiz));
    }
}