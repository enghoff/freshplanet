﻿using UnityEngine;

using TMPro;

using FreshPlanet.Controllers;
using FreshPlanet.Model;

namespace FreshPlanet.Views
{
    public class QuizView : View
    {
        [SerializeField] private TMP_Text _title;
        [SerializeField] private RectTransform _content;
        [SerializeField] private ChoiceEntry _prefab;

        private QuizView Initialize(Quiz quiz, Question question)
        {
            _title.text = quiz.playlist;
            _content.GetComponentsInChildren<ChoiceEntry>().ForEach(choice => Destroy(choice.gameObject));

            question.choices.ForEach(choice => Instantiate(_prefab, _content).Initialize(choice));

            return this;
        }

        public static void Show(Quiz quiz, Question question) => ViewController.Get<QuizView>().Initialize(quiz, question).Show();
    }
}