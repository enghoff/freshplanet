﻿using UnityEngine;

using System.Linq;

using FreshPlanet.Views;

namespace FreshPlanet.Controllers
{
    public class ViewController : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;

        private static View[] _views;

        private void Awake() => _views = _canvas.GetComponentsInChildren<View>(true);

        public static T Get<T>() where T : View => _views.OfType<T>().Single();
        public static void Show(View view) => _views.ForEach(item => item.gameObject.SetActive(item == view));
    }
}