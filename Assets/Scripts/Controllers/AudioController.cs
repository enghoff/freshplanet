﻿using UnityEngine;
using UnityEngine.Networking;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using FreshPlanet.Model;

namespace FreshPlanet.Controllers
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] AudioSource audioSource;

        private static AudioController _instance;
        private static Dictionary<Question, Observable<AudioClip>> _cache = new Dictionary<Question, Observable<AudioClip>>();

        private void Awake() => _instance = this;

        public static void GetAudio(Quiz quiz)
        {
            var questions = quiz.questions.Where(question => !_cache.ContainsKey(question)).ToList();

            questions.ForEach(question => _cache[question] = new Observable<AudioClip>());

            _instance.StartCoroutine(_instance.GetAudio(questions));
        }

        public static void Play(Question question)
        {
            _cache[question].AddListener(clip => _instance.Play(clip), true);
        }

        public static void Stop()
        {
            _instance.audioSource.Stop();
        }

        private void Play(AudioClip audioClip)
        {
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        private IEnumerator GetAudio(IEnumerable<Question> questions)
        {
            foreach (var question in questions)
            {
                using (var www = UnityWebRequestMultimedia.GetAudioClip(question.song.sample, AudioType.WAV))
                {
                    yield return www.SendWebRequest();

                    if (www.isHttpError || www.isNetworkError)
                        Debug.LogError(www.error);
                    else
                        _cache[question].value = DownloadHandlerAudioClip.GetContent(www);
                }
            }
        }
    }
}