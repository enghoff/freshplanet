﻿using UnityEngine;

using System.Collections;

using FreshPlanet.Views;
using FreshPlanet.Model;

namespace FreshPlanet.Controllers
{
    public class GameController : MonoBehaviour
    {
        private static IEnumerator _questions;
        private static Results _results;
        private static Quiz _quiz;

        public static void Play(Quiz quiz)
        {
            _quiz = quiz;
            _questions = quiz.questions.GetEnumerator();
            _results = new Results();

            AudioController.GetAudio(quiz);

            NextQuestion();
        }

        public static void Answer(Choice choice)
        {
            var question = _questions.Current as Question;
            var result = new Result() { Correct = question.choices[question.answerIndex], Player = choice };
            
            _results.Add(result);

            PopupView.Show(result.isCorrect);
        }

        public static void NextQuestion()
        {
            if (_questions.MoveNext())
            {
                AudioController.Play(_questions.Current as Question);
                QuizView.Show(_quiz, _questions.Current as Question);
            }
            else
            {
                AudioController.Stop();
                ResultView.Show(_results);
            }
        }
    }
}