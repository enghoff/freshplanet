﻿using UnityEngine;

using LitJson;

using FreshPlanet.Model;

namespace FreshPlanet.Controllers
{
    public class DataController : MonoBehaviour
    {
        [SerializeField] private TextAsset _jsonConfig;

        public static Config Config { get; private set; }

        private void Awake() => Config = JsonMapper.ToObject<Config>(_jsonConfig.text);
    }
}